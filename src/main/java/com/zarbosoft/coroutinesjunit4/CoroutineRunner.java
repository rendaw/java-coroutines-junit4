package com.zarbosoft.coroutinesjunit4;

import com.zarbosoft.coroutines.Cohelp;
import org.junit.internal.runners.model.ReflectiveCallable;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

import java.lang.reflect.InvocationTargetException;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class CoroutineRunner extends BlockJUnit4ClassRunner {
	public CoroutineRunner(final Class<?> klass) throws InitializationError {
		super(klass);
	}

	private static class CoInvokeMethod extends Statement {
		private final FrameworkMethod testMethod;
		private final CoroutineTestBase target;

		public CoInvokeMethod(final FrameworkMethod testMethod, final Object target) {
			this.testMethod = testMethod;
			this.target = (CoroutineTestBase) target;
		}

		@Override
		public void evaluate() throws Throwable {
			new ReflectiveCallable() {
				@Override
				protected Object runReflectiveCall() throws Throwable {
					Cohelp.block(() -> target.before());
					try {
						Cohelp.block(() -> {
							try {
								testMethod.getMethod().invoke(target);
							} catch (final IllegalAccessException e) {
								throw uncheck(e);
							} catch (final InvocationTargetException e) {
								throw uncheck(e);
							}
						});
					} finally {
						Cohelp.block(() -> target.after());
					}
					return null;
				}
			}.run();
		}
	}

	@Override
	protected Statement methodInvoker(final FrameworkMethod method, final Object test) {
		return new CoInvokeMethod(method, test);
	}
}
