package com.zarbosoft.coroutinesjunit4;

import com.zarbosoft.coroutinescore.SuspendExecution;

public interface CoroutineTestBase {
	/**
	 * Runs before each test method.
	 *
	 * @throws SuspendExecution
	 */
	public void before() throws SuspendExecution;

	/**
	 * Runs after each test method, regardless of exit status.
	 *
	 * @throws SuspendExecution
	 */
	public void after() throws SuspendExecution;
}
